import { By } from "selenium-webdriver";
import * as crypto from "crypto";

export const fillSmallBusinessForm = async (browser, companyName?, email?, password?) => {
	await browser.findElement(By.css(".flex-auto .startnow")).click();
	await browser.findElement(By.css(".md\\3Aw-full .startnow")).click();
	await browser.findElement(By.css(".flex > .w-full > a > .startnow")).click();
	companyName = companyName ?? crypto.randomBytes(10).toString("hex");
	await browser.findElement(By.id("company_name")).click();
	await browser.findElement(By.id("company_name")).sendKeys(companyName);
	await browser.findElement(By.id("country")).click();
	await browser.findElement(By.id("countryfilter")).click();
	await browser.findElement(By.id("countryfilter")).sendKeys("ita");
	await browser.findElement(By.xpath("//div[3]/ul/li/div")).click();
	await browser.findElement(By.id("name")).click();
	const referenceName: string = crypto.randomBytes(10).toString("hex");
	await browser.findElement(By.id("name")).sendKeys(referenceName);
	await browser.findElement(By.id("surname")).click();
	const referenceSurname: string = crypto.randomBytes(10).toString("hex");
	await browser.findElement(By.id("surname")).sendKeys(referenceSurname);
	const phone: number = 11111111;
	await browser.findElement(By.id("phone")).click();
	await browser.findElement(By.id("phone")).sendKeys(phone);
	await browser.findElement(By.id("email")).click();
	email = email ?? crypto.randomBytes(8).toString("hex").concat("@")
		.concat(crypto.randomBytes(6).toString("hex")) + ".test";
	await browser.findElement(By.id("email")).sendKeys(email);
	await browser.findElement(By.css(".pt-2:nth-child(2) #email")).click();
	await browser.findElement(By.css(".pt-2:nth-child(2) #email")).sendKeys(email);
	await browser.findElement(By.id("password")).click();
	password = password ?? email + ".password";
	await browser.findElement(By.id("password")).sendKeys(password);
	await browser.findElement(By.id("terms")).click();
	await browser.findElement(By.css(".pt-2:nth-child(10) .input")).click();
	return { c: companyName, e: email, p: password };
};

export const fillBigBusinessForm = async (browser, companyName?, email?) => {
	await browser.findElement(By.css(".flex-auto .startnow")).click();
	await browser.findElement(By.css(".md\\3Aw-full .contactus")).click();
	companyName = companyName ?? crypto.randomBytes(6).toString("hex");
	await browser.findElement(By.id("companyName")).click();
	await browser.findElement(By.id("companyName")).sendKeys(companyName);
	await browser.findElement(By.id("country")).click();
	await browser.findElement(By.id("countryfilter")).click();
	await browser.findElement(By.id("countryfilter")).sendKeys("ita");
	await browser.findElement(By.xpath("//div[3]/ul/li/div")).click();
	await browser.findElement(By.id("name")).click();
	const referenceName: string = crypto.randomBytes(10).toString("hex");
	await browser.findElement(By.id("name")).sendKeys(referenceName);
	await browser.findElement(By.id("surname")).click();
	const referenceSurname: string = crypto.randomBytes(10).toString("hex");
	await browser.findElement(By.id("surname")).sendKeys(referenceSurname);
	const phone: number = 22222222;
	await browser.findElement(By.id("phone")).click();
	await browser.findElement(By.id("phone")).sendKeys(phone);
	await browser.findElement(By.xpath("//select[@id='employee']")).click();
	await browser.findElement(By.css("#employee>option:nth-child(2)")).click();
	await browser.findElement(By.id("email")).click();
	email = email ?? crypto.randomBytes(8).toString("hex").concat("@")
		.concat(crypto.randomBytes(6).toString("hex")) + ".test";
	await browser.findElement(By.id("email")).sendKeys(email);
	await browser.findElement(By.id("message")).click();
	await browser.findElement(By.id("message")).sendKeys("TEST MESSAGE");
	await browser.findElement(By.id("terms")).click();
	return { c: companyName, e: email };
};
