import { Builder, By, until } from "selenium-webdriver";
import * as chrome from "selenium-webdriver/chrome";
import * as process from "process";
import { fillBigBusinessForm, fillSmallBusinessForm } from "./helper";

const options = new chrome.Options();
const chromeOptions = process.env.GITHUB_ACTIONS ? options.headless() : options;

describe("Creation new Smart business user (Starter)", () => {
	let browser;
	
	beforeEach(async () => {
		browser = await new Builder()
			.forBrowser("chrome")
			.setChromeOptions(chromeOptions)
			.build();
		return await browser.manage().setTimeouts({ implicit: 30000 });
	});
	
	afterEach(() => {
		return setTimeout(() => {
			browser.quit();
		}, 500);
	});
	
	let companyName;
	let email;
	let password;
	
	it("Submit form smart business (Starter) ", async () => {
		await browser.get(`${process.env.BUSINESS_BASE_URL}`);
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.className("iubenda-cs-accept-btn"))));
		await browser.findElement(By.className("iubenda-cs-accept-btn")).click();
		const title: string = await browser.getTitle();
		await browser.manage().window().setRect({ x: 0, y: 0, width: 1920, height: 1177 });
		await expect(title).toEqual("Business | Treedom");
		const { c, e, p } = await fillSmallBusinessForm(browser);
		companyName = c;
		email = e;
		password = p;
		await browser.findElement(By.id("signup")).click();
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.xpath("//div[@id='modal-success']/div/div/div[2]"))));
		await browser.findElement(By.xpath("//div[@id='modal-success']/div/div/div[2]/button")).click();
		const redirected = await browser.wait(until.urlIs(`${process.env.WWW_BASE_URL}/plant-a-tree`));
		await expect(redirected);
	});
	
	it("Login", async () => {
		await browser.get(`${process.env.WWW_BASE_URL}`);
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.css(".tr-button--secondary:nth-child(1)"))));
		await browser.findElement(By.css(".tr-button--secondary:nth-child(1)")).click();
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.xpath("//input[@name='check.username']")))).click();
		await browser.findElement(By.xpath("//input[@name='check.username']")).sendKeys(email);
		await browser.findElement(By.css(".btn-success > strong")).click();
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.xpath("//input[@name='signin.password']")))).click();
		await browser.findElement(By.xpath("//input[@name='signin.password']")).sendKeys(password);
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.xpath("//button[@name='signin.submit']")))).click();
		const redirected = await browser.wait(until.urlIs(`${process.env.WWW_BASE_URL}/organization/${companyName}`));
		await expect(redirected);
	});
	
	it("Submit form big business (Premium) ", async () => {
		await browser.get(`${process.env.BUSINESS_BASE_URL}`);
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.className("iubenda-cs-accept-btn"))));
		await browser.findElement(By.className("iubenda-cs-accept-btn")).click();
		const title: string = await browser.getTitle();
		await browser.manage().window().setRect({ x: 0, y: 0, width: 1920, height: 1177 });
		await expect(title).toEqual("Business | Treedom");
		const { c, e } = await fillBigBusinessForm(browser);
		companyName = c;
		email = e;
		await browser.findElement(By.xpath("//button[@id='signup']")).click();
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.xpath("//div[@id='modal-success']/div/div/div[2]"))));
		await browser.findElement(By.xpath("//div[@id='modal-success']/div/div/div[2]/button")).click();
		const redirected = await browser.wait(until.urlIs(`${process.env.BUSINESS_BASE_URL}`));
		await expect(redirected);
	});
});
