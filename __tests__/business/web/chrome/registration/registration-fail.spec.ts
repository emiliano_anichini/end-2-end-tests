import { Builder, By, until, WebElement } from "selenium-webdriver";
import * as chrome from "selenium-webdriver/chrome";
import * as process from "process";
import * as crypto from "crypto";
import { fillSmallBusinessForm } from "./helper";

const options = new chrome.Options();
const chromeOptions = process.env.GITHUB_ACTIONS ? options.headless() : options;

describe("Creation duplicated Smart business user (Starter)", () => {
	let browser;
	
	beforeEach(async () => {
		browser = await new Builder()
			.forBrowser("chrome")
			.setChromeOptions(chromeOptions)
			.build();
		return await browser.manage().setTimeouts({ implicit: 30000 });
	});
	
	afterEach(() => {
		return setTimeout(() => {
			browser.quit();
		}, 500);
	});
	
	let globalCompanyName;
	let globalEmail;
	let globalPassword;
	
	it("Submit form smart business (Starter) ", async () => {
		await browser.get(`${process.env.BUSINESS_BASE_URL}`);
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.className("iubenda-cs-accept-btn"))));
		await browser.findElement(By.className("iubenda-cs-accept-btn")).click();
		const title: string = await browser.getTitle();
		await browser.manage().window().setRect({ x: 0, y: 0, width: 1920, height: 1177 });
		await expect(title).toEqual("Business | Treedom");
		const { c, e, p } = await fillSmallBusinessForm(browser);
		globalCompanyName = c;
		globalEmail = e;
		globalPassword = p;
		await browser.findElement(By.id("signup")).click();
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.xpath("//button[@class='text-base startnow']"))));
		await browser.findElement(By.xpath("//button[@class='text-base startnow']"))
			.click();
		const redirected = await browser.wait(until.urlIs(`${process.env.WWW_BASE_URL}/plant-a-tree`));
		await expect(redirected);
	});
	
	it("Submit form smart business already registered (Starter) ", async () => {
		await browser.get(`${process.env.BUSINESS_BASE_URL}`);
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.className("iubenda-cs-accept-btn"))));
		await browser.findElement(By.className("iubenda-cs-accept-btn")).click();
		const title: string = await browser.getTitle();
		await browser.manage().window().setRect({ x: 0, y: 0, width: 1920, height: 1177 });
		await expect(title).toEqual("Business | Treedom");
		const url = await browser.getCurrentUrl();
		await fillSmallBusinessForm(browser, globalCompanyName, globalEmail);
		await browser.findElement(By.id("signup")).click();
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.css(".btn_error"))));
		await browser.findElement(By.css(".btn_error")).click();
		expect(await browser.getCurrentUrl() === url);
	});
	
	it("Check mandatory fields on registration smart business (Starter) ", async () => {
		await browser.get(`${process.env.BUSINESS_BASE_URL}`);
		await browser.wait(
			until.elementIsVisible(browser.findElement(By.className("iubenda-cs-accept-btn"))));
		await browser.findElement(By.className("iubenda-cs-accept-btn")).click();
		const title: string = await browser.getTitle();
		await browser.manage().window().setRect({ x: 0, y: 0, width: 1920, height: 1177 });
		await expect(title).toEqual("Business | Treedom");
		await browser.findElement(By.css(".flex-auto .startnow")).click();
		await browser.findElement(By.css(".md\\3Aw-full .startnow")).click();
		await browser.findElement(By.css(".flex > .w-full > a > .startnow")).click();
		await browser.findElement(By.id("company_name")).click();
		const companyNameErr: WebElement = await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/div[1]/div[2]/span/span"));
		expect(companyNameErr.isDisplayed());
		expect(await companyNameErr.getText() === "Campo obbligatorio");
		await browser.findElement(By.id("country")).click();
		await browser.findElement(By.id("countryfilter")).click();
		const countryErr: WebElement = await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/div[2]/div[2]/span/span"));
		expect(countryErr.isDisplayed());
		expect(await countryErr.getText() === "Campo obbligatorio");
		await browser.findElement(By.id("name")).click();
		const nameErr: WebElement = await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/div[4]/div[2]/span/span"));
		expect(nameErr.isDisplayed());
		expect(await nameErr.getText() === "Campo obbligatorio");
		await browser.findElement(By.id("surname")).click();
		const surnameErr: WebElement = await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/div[5]/div[2]/span/span"));
		expect(surnameErr.isDisplayed());
		expect(await surnameErr.getText() === "Campo obbligatorio");
		const phone: number = 12345;
		await browser.findElement(By.id("phone")).click();
		await browser.findElement(By.id("phone")).sendKeys(phone);
		const phoneFormatErr: WebElement = await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/div[6]/div[2]/span/span"));
		expect(phoneFormatErr.isDisplayed());
		expect(await phoneFormatErr.getText() === "Formato non valido");
		await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/span[1]/div[1]/div[2]/span/input"))
			.click();
		const emailErr: WebElement = await browser.findElement(By.id("email_msg"));
		expect(emailErr.isDisplayed());
		expect(await emailErr.getText() === "Campo obbligatorio");
		let email = crypto.randomBytes(6).toString("hex");
		await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/span[1]/div[1]/div[2]/span/input"))
			.sendKeys(email);
		expect(await emailErr.getText() === "Formato email non valido");
		email = email.concat("@").concat(crypto.randomBytes(6).toString("hex")) + ".test";
		const confirmEmail: WebElement = await browser.findElement(By.xpath("//html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/span[1]/div[1]/div[2]/span/input"));
		await confirmEmail.sendKeys(email);
		await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/span[1]/div[2]/div[2]/span/input"))
			.click();
		const confirmEmailErr: WebElement = await browser.findElement(By.id("confirm_email_msg"));
		expect(await confirmEmailErr.getText() === "Campo obbligatorio");
		await browser.findElement(By.xpath("/html/body/div/div[3]/div/div/div/div/div/div/span/form/div[2]/span[1]/div[2]/div[2]/span/input"))
			.sendKeys(email.slice(0, 3));
		expect(await confirmEmailErr.getText() === "Le mail devono coincidere");
		await browser.findElement(By.id("password")).click();
		const passwordErr: WebElement = await browser.findElement(By.id("password"));
		expect(await passwordErr.getText() === "Campo obbligatorio");
		let password = crypto.randomBytes(7).toString("hex");
		await browser.findElement(By.id("password")).sendKeys(password);
		expect(await passwordErr.getText() === "La password deve essere di almeno 8 caratteri");
		await browser.findElement(By.id("terms")).click();
		await browser.findElement(By.id("terms")).click();
		const termsError: WebElement = await browser.findElement(By.id("terms_msg"));
		expect(termsError.isDisplayed());
	});
})
;
